package Controller;

import Model.Players.ComputerPlayer;
import Model.Players.Player;
import Model.Structure.Card;
import Model.Structure.Game;
import View.TUI;

import java.util.ArrayList;

public class UnoMainViewController {

    public static void  main(String[] args) throws InterruptedException {
        TUI view = new TUI();
        System.out.println("program initiated");

        ArrayList<Player> players = new ArrayList<>();
        System.out.println("player list created");

        players.add(new ComputerPlayer("player1"));
        players.add(new ComputerPlayer("player2"));

        System.out.println("players created");

        Game game = new Game(players);
        for (Player p : players){
            p.setGame(game);
            game.setPlayerMap(p);
        }
        System.out.println("game created");

        System.out.println(game.getDrawPile().get(0).getColor());
        game.dealCardsAndShuffle(game.getDrawPile(), players);  //shuffle the cards and deal them to the players
        System.out.println("cards divided");

        view.printDeck(players.get(0));
        view.printDeck(players.get(1));


        System.out.println(game.getPlayerList() + "players");
        System.out.println(game.getRecentCard().getColor());
        System.out.println(game.getRecentCard().getValue());

        game.setMostRecentCard(game.getDrawPile().get(0));
        System.out.println(game.getRecentCard().getColor());
        System.out.println(game.getRecentCard().getValue());


        players.get(0).makeMove();
        Thread.sleep(50);
        while (!game.isOver()){
            game.decideTurn(game.getRecentCard());

            System.out.println("Player " + game.getPlayerMap().get(game.getCurrentPlayer()).getName() + " is now playing");
            Thread.sleep(50);

            view.printDeck(game.getPlayerMap().get(game.getCurrentPlayer()));
            Thread.sleep(50);

            game.getPlayerMap().get(game.getCurrentPlayer()).makeMove();

            System.out.println("New card on top: " + view.cardString(game.getRecentCard()));

            if ((game.getRecentCard().getSpecial() == Card.Special.TAKE_TWO || game.getRecentCard().getSpecial() == Card.Special.TAKE_FOUR) &&
                players.get(game.currentPlayer).getPlayable().size() != 0) {
                game.DrawCards(game.getRecentCard());
                Thread.sleep(50);
            }


            System.out.println("Updated deck from player: ");
            view.printDeck(game.getPlayerMap().get(game.getCurrentPlayer()));
            Thread.sleep(50);


            Thread.sleep(50);

            if (game.isOver()) {
                game.calculateScore(players.get(game.currentPlayer).getScore(), players.get(game.currentPlayer));
                System.out.println("Player score is: " + game.getPlayerMap().get(game.getCurrentPlayer()).getScore());
            }
        }
        System.out.println("game over!");
    }

}
