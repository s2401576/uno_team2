package Controller;

import Model.Server_Client.Client;
import Model.Exceptions.InvalidMoveException;
import Model.Structure.Card;
import Model.Players.ComputerPlayer;
import Model.Players.HumanPlayer;
import Model.Players.Player;
import Model.Structure.Game;
import Protocol.Protocol;
import Model.Server_Client.UnoServer;

import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class ClientHandler implements Runnable{

    private Socket clientSocket;
    private BufferedReader in;
    private BufferedWriter out;
    private UnoServer server;
    private String name;
    private int numberOfDrawCards = 1;

    public ClientHandler(Socket clientSocket, UnoServer server){
        try {
            in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            out = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream()));
            this.clientSocket = clientSocket;
            this.server = server;

            //String command = in.readLine();
           // String[] words = command.split(Protocol.SEPERATOR);
            //System.out.println(command);
           // out.write(words[2] + Protocol.SEPERATOR + words[3]);
            //out.write(server.WelcomeMsg(words[2], Protocol.Flags.valueOf(words[3].toUpperCase(Locale.ROOT))));
            //out.flush();
            //out.write("Ready?");
            //System.out.println(server.WelcomeMsg(words[1], Protocol.Flags.valueOf(words[2].toUpperCase(Locale.ROOT))));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }



    @Override
    public void run() {
        String msg;
        try {
            while(true) {
                //System.out.println("looking for msg");
                msg = in.readLine();
                System.out.println("Incoming message: " + msg);
                if (msg != null){
                    handleCommand(msg);
                }
                //msg = in.readLine();
            }

            //shutdown();
        } catch (IOException | InterruptedException | InvalidMoveException e) {
            System.out.println(e.getMessage());
            //shutdown();
        }

    }

    public void handleCommand(String command) throws IOException, InterruptedException, InvalidMoveException {
        String[] words = command.split(Protocol.SEPERATOR);

        if (isInteger(words[1])){

            if (Integer.parseInt(words[1]) == 0){
                server.game.getCurrentPlayerObject().takeCard();
                sendToEveryone(server.MoveBC(server.game.getCurrentPlayerObject().getName(), false, null));



                server.game.decideTurn(server.game.getRecentCard());
                System.out.println("now player " + server.game.getCurrentPlayerObject().getName() + " has the turn");
                System.out.println("this is the deck: " + server.view.getPlayerDeckString(server.game.getCurrentPlayerObject().getDeck()));
                server.nextPlayer();
            } else {
                System.out.println("card number " + words[1] + " is being played");
                try {
                    if (!server.game.isValid(server.game.getCurrentPlayerObject().getDeck().get(Integer.parseInt(words[1])-1))){
                        throw new InvalidMoveException("This move is not allowed");
                    }
                    //update the most recent card in game
                    server.game.setMostRecentCard(server.game.getCurrentPlayerObject().getDeck().get(Integer.parseInt(words[1])-1));
                    //do the actual move
                    server.game.getCurrentPlayerObject().placeCard(server.game.getCurrentPlayerObject().getDeck().get(Integer.parseInt(words[1])-1));
                    //announce the move that has been made
                    sendToEveryone(server.MoveBC(server.game.getCurrentPlayerObject().getName(), true, server.game.getRecentCard()));

//                    if (server.game.getRecentCard().getSpecial() == Card.Special.COLOR) {
//                        sendMessage("Please enter color: ");
//                        try {
//                            server.game.setCardColour(Card.Color.valueOf(in.readLine()));
//                            if () throw new UnknownMessageException("Not a valid color");
//                        } catch (UnknownMessageException e) {
//                            e.getMessage();
//                        }
//
//                    }

                    //check if the played card is a color special card
                    choosingColor();

                    // if played card is special plus or something
                    //server.game.DrawCards(server.game.getRecentCard()); //make players take cards

                    // if played card is a +2 or +4
                    if (server.game.getRecentCard().getSpecial() == Card.Special.TAKE_FOUR || server.game.getRecentCard().getSpecial() == Card.Special.TAKE_TWO) {
                        if (server.game.normal) {
                            server.game.DrawCards(server.game.getRecentCard()); //make players take cards
                        }

                        //for progressive gamemode the following rules apply
                        if (server.game.progressive) {
                            int currentplay = server.game.getCurrentPlayer();
                            server.game.decideTurn(server.game.getRecentCard());//find the nextplayer and change the value of currentplayer
                            boolean hasDrawCard = false;

                            //check whether the next player has a +4 or +2 in their hands which can be stacked on the previous +2 or +4
                            for (Card card: server.game.getCurrentPlayerObject().getDeck()) {
                                if (card.getSpecial() == Card.Special.TAKE_FOUR && server.game.getRecentCard().getSpecial() == Card.Special.TAKE_FOUR) {
                                    numberOfDrawCards++;
                                    hasDrawCard = true;
                                    break;
                                }
                                if (card.getSpecial() == Card.Special.TAKE_TWO && server.game.getRecentCard().getSpecial() == Card.Special.TAKE_TWO) {
                                    numberOfDrawCards++;
                                    hasDrawCard = true;
                                    break;
                                }
                            }

                            //if the next player does not have a card that can be stacked, the next player will have to draw cards
                            if (!hasDrawCard) {
                                server.game.setCurrenplayer(currentplay); //set the currentplayer back to the actual currentplayer
                                for (int i =0; i < numberOfDrawCards; i++) {
                                    server.game.DrawCards(server.game.getRecentCard()); //draw cards for the next player for the amount of times that the cards have been stacked
                                }
                                numberOfDrawCards = 1; //set the stack value back to 1
                            }
                            server.game.setCurrenplayer(currentplay);
                        }
                    }





                    //do a quick check to see if the game is done
                    if (server.game.isOver()){
                        server.finalizeGame(server.game);
                        sendToEveryone(server.RoundWinnerBC(server.game.getCurrentPlayerObject().getName(), true,  server.game.gameScore(server.game.getCurrentPlayerObject())));
                    }

                    //if the game is not over yet, then decide who has the next turn
                    server.game.decideTurn(server.game.getRecentCard());
                    //System.out.println("now player " + server.game.getCurrentPlayerObject().getName() + " has the turn");
                    sendToEveryone("The current card on top is: " + server.StructCard(server.game.getRecentCard()));
                    sendToEveryone(server.game.getCurrentPlayerObject().getName() + ", it is your turn!");
                    //System.out.println("this is the deck: " + server.view.getPlayerDeckString(server.game.getCurrentPlayerObject().getDeck()));

                    //next player gets to make a move
                    server.nextPlayer();

                    //some people play cards that are not allowed
                    //an exception will be thrown, and they get another chance
                } catch (InvalidMoveException e){
                    //handleCommand(command);
                    //System.out.println(e.getMessage());
                    sendMessage("Invalid move, please choose again or take a card.");
                }


            }

        }

        switch (words[1].toLowerCase(Locale.ROOT)) {
            case "announce":
               this.name = words[2];
               if(words.length > 4){
                    if (words[4].equalsIgnoreCase("computer")){
                        server.addToPlayerList(new ComputerPlayer(words[2]));
                    }
               } else{
                   server.addToPlayerList(new HumanPlayer(words[2]));
               }
                System.out.println("creating player with name " + words[2]);
               //server.addToPlayerList(new HumanPlayer(words[2]));
                System.out.println("player created");
               sendMessage(server.WelcomeMsg(words[2], Protocol.Flags.valueOf(words[3].toUpperCase(Locale.ROOT))));
               break;
            case "canwestart":
                sendToEveryone(server.StartQBC());
                server.lastMessage = server.StartQBC();
                Client.lastQuestion = server.StartQBC();
                break;
            case "yes":
                if (server.lastMessage.equals(server.StartQBC())){
                    server.wantsToStart += 1;
                    if(server.ready()){
                        System.out.println("attempting start game");
                        sendToEveryone("Starting the game...");
                        server.startGame();
                        System.out.println("game started");
                        System.out.println("these are the players");
                        System.out.println(server.game.getPlayerList());
                        //System.out.println("players are: " + getPlayerNames(server.game.getPlayerList()));
                        //System.out.println("current color is " + server.game.getRecentCard().getColor().name());
                        //System.out.println("current value is " + Integer.toString(server.game.getRecentCard().getValue()));
                        sendToEveryone(server.StartBC(true, server.game.getRecentCard(), getPlayerNames(server.game.getPlayerList())));

                    }
                } else{
                    break;
                }
            default:
                out.write(command);
                break;
        }




//        if (command.equalsIgnoreCase("hello")){
//            out.write("hello client");
//        }
//
//        if (command.equalsIgnoreCase("Can we start?")){
//            out.write(server.CanWeStart());
//        }
//
//        if (words[1].equalsIgnoreCase("announce")){
//            name = words[2];
//            out.write("Hello " + name);
//        }
//
//        if (command.equals("Yes")){
//            out.write("Thank you");
//        }
    }

    public void choosingColor() throws IOException {
        if (server.lastMessage.equalsIgnoreCase("ChooseColor")){
            sendMessage("Choose from the following colors: ");
            sendMessage("RED = 1, GREEN = 2, BLUE = 3, YELLOW = 4");
            String str = in.readLine();
            switch (Integer.parseInt(str)){
                case 1:
                    server.game.setCardColour(Card.Color.RED);
                    sendToEveryone("Chosen color: " + Card.Color.RED);
                    break;
                case 2:
                    server.game.setCardColour(Card.Color.GREEN);
                    sendToEveryone("Chosen color: " + Card.Color.GREEN);
                    break;
                case 3:
                    server.game.setCardColour(Card.Color.BLUE);
                    break;
                case 4:
                    server.game.setCardColour(Card.Color.YELLOW);
                    break;
                default:
                    sendMessage("Not a valid number");
            }
            server.lastMessage = "Standard";
        }
    }

    public boolean isInteger(String str){
        for (int i = 0; i < 99; i++){
            if (str.equals(Integer.toString(i))){
                return true;
            }
        }
        return false;
    }


    public void sendToEveryone(String msg){
        for (ClientHandler c : server.handlerList){
            c.sendMessage(msg);
        }
    }

    public void sendMessage(String msg)  {
        try{
            out.write(msg);
            out.newLine();
            out.flush();
        } catch (IOException e){
            System.out.println("Could not send this message");
            e.printStackTrace();
        }

    }

    public String getName(){
        return this.name;
    }

    public List<String> getPlayerNames(List<Player> players){
        ArrayList<String> result = new ArrayList<>();
        for (Player p : players){
            result.add(p.getName());
        }
        return result;
    }



    private void shutdown() {
        System.out.println("> [" + name + "] Shutting down.");
        try {
            in.close();
            out.close();
            clientSocket.close();
            System.out.println("connections closed");
        } catch (IOException e) {
            e.printStackTrace();
        }
        server.removeClient(this);

    }

}
