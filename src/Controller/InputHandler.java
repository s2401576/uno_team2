package Controller;

import Model.Server_Client.Client;
import Protocol.Protocol;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

public class InputHandler implements Runnable{

    private Socket sock;
    private BufferedReader in;
    public String latestMessage = "hallo";
    private Client client;

    public InputHandler(Socket s, Client c) throws IOException {
        sock = s;
        in = new BufferedReader(new InputStreamReader(s.getInputStream()));
        this.client = c;
    }


    @Override
    public void run() {
            //String serverMsg = null;
            try {
                while (true) {
                    String serverMsg = in.readLine();
                    if (serverMsg == null) break;
                    setLatestMessage(serverMsg);
                    System.out.println("Server says the following: " + serverMsg);
                    if (!client.isHuman()){
                        respond(serverMsg);
                    }

                }
            } catch (IOException e){
                System.out.println("A problem occurred, please check the connection.");;
                System.out.println(e.getMessage());
            } finally {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }



        public void setLatestMessage(String message){
            this.latestMessage = message;
        }

        public String getLatestMessage(){
            return this.latestMessage;
        }

        public void respond(String serverMsg) {
            String[] words = serverMsg.split(Protocol.SEPERATOR);

            switch (words[1]) {
                case "YTurn":
                    client.makeMove();
                    break;
                case "ChooseColor":
                    client.chooseColor();
                    break;
            }
        }

    }




