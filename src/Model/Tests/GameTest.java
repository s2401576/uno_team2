package Model.Tests;

import Model.Structure.Card;
import Model.Players.ComputerPlayer;
import Model.Structure.Game;
import Model.Players.Player;
import org.junit.jupiter.api.*;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class GameTest {

    //the game and playerlist should be accessible for testing purposes, so not in the BeforeEach statement.
    Game game;
    ArrayList<Player> players = new ArrayList<>();


    @BeforeEach
    void StartGame() {

        //create new players and add them to the playerlist
        players.add(new ComputerPlayer("player1"));
        players.add(new ComputerPlayer("player2"));
        players.add(new ComputerPlayer("player3"));

        //add players to the game for testing purposes
        game = new Game(players);
        for (Player p : players){
            p.setGame(game);
            game.setPlayerMap(p);
        }
    }



    @org.junit.jupiter.api.Test
    @DisplayName("generate the right number of cards")
    void generatePile() {
        ArrayList<Card> test = game.generatePile();
        assertEquals(108, test.size()); //test if all the cards were made
        for (Card c: test) {
            assertNotNull(c); //test if there are no cards which are null
        }
    }

    @org.junit.jupiter.api.Test
    @DisplayName("Deal the right number of cards and make sure to delete them from the drawpile")
    void dealCardsAndShuffle() {
        game.dealCardsAndShuffle(game.getDrawPile(), players);

        //check that when dealing everyone gets 7 cards
        assertEquals(7, players.get(0).getDeck().size());
        assertEquals(7, players.get(1).getDeck().size());
        assertEquals(7, players.get(2).getDeck().size());

        //test whether these cards get removed from the drawpile
        assertEquals(108-21, game.getDrawPile().size());
    }

    @Test
    @DisplayName("take Card and add it to layer hand")
    void takeonecardplayer() {
        game.takeOneCardPlayer(players.get(game.currentPlayer));

        // test that a card was added the the deck of the player
        assertEquals(1, players.get(game.currentPlayer).getDeck().size());

        // tets that a card was removed from the deck
        assertEquals(107, game.getDrawPile().size());
    }

    @Test
    @DisplayName("take card and return this card")
    void takeonecard() {
        ArrayList<Card> test =new ArrayList<>();
        test.add(game.takeOneCard());

        assertEquals(1, test.size()); // test if the function returned one card
        assertEquals(107, game.getDrawPile().size()); // test if the function removed the card from the drawpile
        test.clear();

        //test the funciton with larger numbers
        for (int i = 0; i < 8; i++) {
            test.add(game.takeOneCard());
        }
        assertEquals(8, test.size());
        assertEquals(99, game.getDrawPile().size());
    }

    @Test
    @DisplayName("Deciding the turn of the player")
    void decideTurn() {
        Card card = new Card(Card.Color.RED, Card.Special.NONE, 9);
        game.setCurrenplayer(0);

        //check if the next player gets the turn (from player 0 to player 1)
        game.decideTurn(card);
        assertEquals(1, game.currentPlayer);

        //check whether it will go back to the first player after it reaches the end of the playerlist
        game.decideTurn(card);
        game.decideTurn(card);
        assertEquals(0, game.currentPlayer);

        //check whether a reverse card changes the direction and gives the player in the opposite direction the turn
        Card card1 = new Card(Card.Color.RED, Card.Special.REVERSE, 20);
        game.decideTurn(card1);
        assertEquals(2, game.currentPlayer);

        //check whether the direction is still in the correct way and goes to the right player (one player to the left so player 1)
        game.decideTurn(card);
        assertEquals(1, game.currentPlayer);
    }

    @Test
    @DisplayName("Drawing 4 cards")
    void DrawCards() {
        Card drawFour = new Card(Card.Color.WILD, Card.Special.TAKE_FOUR, 50);

        //draw four functionality to the right (with current player being 0)
        game.DrawCards(drawFour);
        assertEquals(4, players.get(1).getDeck().size());

        //draw four when at the end of the player list and direction to the right
        game.setCurrenplayer(2);
        game.DrawCards(drawFour);
        assertEquals(4, players.get(0).getDeck().size());

        //draw four when direction is left
        game.setDirectionleft();
        game.DrawCards(drawFour);
        assertEquals(8, players.get(1).getDeck().size()); //expect 8 since we already drew 4 cards previously

        //draw four when at the end of the player list with direction the the left
        game.setCurrenplayer(0);
        game.DrawCards(drawFour);
        assertEquals(4, players.get(2).getDeck().size());
    }

    @Test
    @DisplayName("Drawing 2 cards")
    void DrawTwoCards() {
        Card drawTwo = new Card(Card.Color.RED, Card.Special.TAKE_TWO, 20);

        //draw two functionality to the right (with current player being 0)
        game.DrawCards(drawTwo);
        assertEquals(2, players.get(1).getDeck().size());

        //draw two when at the end of the player list and direction to the right
        game.setCurrenplayer(2);
        game.DrawCards(drawTwo);
        assertEquals(2, players.get(0).getDeck().size());

        //draw two when direction is left
        game.setDirectionleft();
        game.DrawCards(drawTwo);
        assertEquals(4, players.get(1).getDeck().size()); //expect 4 since we already drew 2 cards previously

        //draw two when at the end of the player list with direction to the left
        game.setCurrenplayer(0);
        game.DrawCards(drawTwo);
        assertEquals(2, players.get(2).getDeck().size());
    }


    @org.junit.jupiter.api.Test
    @DisplayName("Skipping logic")
    void skip() {
        game.setCurrenplayer(0);
        game.setDirectionright();

        //check whether skip works when the direction is to the right
        game.skip();
        assertEquals(2, game.currentPlayer); // check when it is in a normal situation (not end of list)
        game.skip();
        assertEquals(1, game.currentPlayer);//check when it is at the end of the list
        game.skip();
        assertEquals(0, game.currentPlayer);// check when it is 1 away from the end of the list

        //check whether skip works when the direction is to the left
        game.setDirectionleft();
        game.skip();
        assertEquals(1, game.currentPlayer);// check when it is at the end of the list
        game.skip();
        assertEquals(2, game.currentPlayer);// check when it is 1 before the end of the list of players
        game.skip();
        assertEquals(0, game.currentPlayer);//check in a situation where it is not at the end of the list (normal situation)
    }

    @Test
    @DisplayName("Discard cards to the discard pile")
    void Discard() {
        Card card = new Card(Card.Color.RED, Card.Special.NONE, 9);
        game.discard(card);
        assertEquals(1 , game.getDiscardPile().size()); //check if the size of the discardpile has increased by 1
    }

    @org.junit.jupiter.api.Test
    @DisplayName("Testing if card is valid")
    void isValid() {
        Card drawTwo = new Card(Card.Color.RED, Card.Special.TAKE_TWO, 20);
        Card drawFour = new Card(Card.Color.WILD, Card.Special.TAKE_FOUR, 50);
        Card red9 = new Card(Card.Color.RED, Card.Special.NONE, 9);
        Card red5 = new Card(Card.Color.RED, Card.Special.NONE, 5);
        Card green9 = new Card(Card.Color.GREEN, Card.Special.NONE, 9);
        Card green5 = new Card(Card.Color.GREEN, Card.Special.NONE, 5);
        Card skipR = new Card(Card.Color.RED, Card.Special.SKIP, 20);
        Card skipB = new Card(Card.Color.BLUE, Card.Special.SKIP, 20);
        Card reverseR = new Card(Card.Color.RED, Card.Special.REVERSE, 20);
        Card reverseY = new Card(Card.Color.YELLOW, Card.Special.REVERSE, 20);
        Card color = new Card(Card.Color.WILD, Card.Special.COLOR, 50);

        //drawfour functionality
        game.setMostRecentCard(drawFour);
        assertFalse(game.isValid(drawFour)); //drawfour can't stack
        assertFalse(game.isValid(drawTwo)); //drawtwo can't go on draw 4
        assertFalse(game.isValid(drawFour)); //drawfour can't go on draw 2

        game.setMostRecentCard(skipR);
        assertTrue(game.isValid(drawFour)); //draw four can go on special cards

        //drawfour and color card functionality
        game.setMostRecentCard(red9);
        assertTrue(game.isValid(drawFour)); //drawfour can always be played on normal cards
        assertTrue(game.isValid(color)); //color can be played on wild cards
        assertTrue(game.isValid(color)); //color can be stacked

        game.setMostRecentCard(red9);
        assertTrue(game.isValid(color)); //color can be played on normal cards

        game.setMostRecentCard(skipR);
        assertTrue(game.isValid(color)); //color can be played on special cards


        game.setMostRecentCard(drawTwo);
        assertFalse(game.isValid(drawTwo)); //draw two can't stack

        game.setMostRecentCard(red9);
        assertTrue(game.isValid(drawTwo)); //draw two can be placed on same color as itself (see color of drawtwo above)

        game.setMostRecentCard(green5);
        assertFalse(game.isValid(drawTwo)); //drawtwo cannot be played on different color than itself

        //check normal card functionality
        game.setMostRecentCard(red9);
        assertTrue(game.isValid(red5)); //red should go on red
        assertTrue(game.isValid(green9)); //green should go on red since same number
        assertFalse(game.isValid(green5)); //should not stack since not same number and color

        //check special card functionality
        game.setMostRecentCard(skipR);
        assertTrue(game.isValid(skipB)); //all special cards can stack (except +2 and +4 which were tested before)
        assertFalse(game.isValid(reverseY)); //different special cards should not stack

        game.setMostRecentCard(skipR);
        assertTrue(game.isValid(reverseR)); //same colour so they should be valid

    }

    @Test
    @DisplayName("Progressive UNO")
    void progressiveTest() {
        game.progressive = true;
        Card drawTwo = new Card(Card.Color.RED, Card.Special.TAKE_TWO, 20);
        Card drawFour = new Card(Card.Color.WILD, Card.Special.TAKE_FOUR, 50);
        game.setMostRecentCard(drawTwo); //set recent card to +2 to see if +2 will stack

        assertEquals(true, game.isValid(drawTwo)); //should be true since +2 can now stack
        assertEquals(false, game.isValid(drawFour));// should be false since +4 and +2 cannot stack

        game.setMostRecentCard(drawFour); //try to stack +4

        assertEquals(true, game.isValid(drawFour));//stack +4

    }

    @Test
    @DisplayName("Reshuffle cards when drawpile is empty")
    void reshuffle() {
        Card red9 = new Card(Card.Color.RED, Card.Special.NONE, 9);
        game.getDrawPile().clear(); //clear drawpile for testing purposes

        //add 20 cards to discardpile
        for (int i = 0; i < 20; i++) {
            game.getDiscardPile().add(red9);
        }
        game.reshuffle();

        //there should now be 20 cards in the drawpile and 0 in discardpile
        assertEquals(20, game.getDrawPile().size());
        assertEquals(0, game.getDiscardPile().size());

        //add 10 more cards to discardpile
        for (int i = 0; i < 10; i++) {
            game.getDiscardPile().add(red9);
        }
        game.reshuffle();

        //there should be 30 cards in the drawpile and 0 in the discardpile
        assertEquals(30, game.getDrawPile().size());
        assertEquals(0, game.getDiscardPile().size());
    }

    @Test
    @DisplayName("check if game is over")
    void isOver() {
        //game should not be over since all players have cards
        game.dealCardsAndShuffle(game.getDrawPile(), players);
        assertFalse(game.isOver());

        //clear the deck of a random player and see if it notices that the game should be over
        players.get(2).getDeck().clear();
        assertTrue(game.isOver());
    }

}