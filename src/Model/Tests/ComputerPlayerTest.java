package Model.Tests;

import Model.Players.ComputerPlayer;
import Model.Players.Player;
import Model.Structure.Card;
import Model.Structure.Game;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ComputerPlayerTest {
    //the game and playerlist should be accessible for testing purposes, so not in the BeforeEach statement.
    Game game;
    ArrayList<Player> players = new ArrayList<>();


    @BeforeEach
    void StartGame() {

        //create new players and add them to the playerlist
        players.add(new ComputerPlayer("player1"));
        players.add(new ComputerPlayer("player2"));
        players.add(new ComputerPlayer("player3"));

        //add players to the game for testing purposes
        game = new Game(players);
        for (Player p : players){
            p.setGame(game);
            game.setPlayerMap(p);
        }
    }

    @Test
    @DisplayName("choose A Random Colour")
    void choosecolor() {
        players.get(game.currentPlayer).chooseColour(0); //choose blue color
        assertEquals(Card.Color.BLUE, game.getRecentCard().getColor()); //check if it is blue

        players.get(game.currentPlayer).chooseColour(1);//do the same for another color
        assertEquals(Card.Color.YELLOW, game.getRecentCard().getColor());
    }

    @Test
    @DisplayName("place a card")
    void placeCard() throws InterruptedException {
        Card card = new Card(Card.Color.RED, Card.Special.NONE, 9);

        players.get(game.currentPlayer).placeCard(card);
        assertEquals(1, game.getDiscardPile().size()); //should add card to discardpile
        assertEquals(card, game.getRecentCard()); //new card on top should be the card that was played
    }

    @Test
    @DisplayName("take card from drawpile")
    void takeCard() {
        players.get(game.currentPlayer).takeCard();
        assertEquals(107, game.getDrawPile().size());
        assertEquals(1, game.getCurrentPlayerObject().getDeck().size());
    }
}
