package Model.Tests;

import Model.Server_Client.Client;
import Model.Server_Client.UnoServer;
import org.junit.jupiter.api.*;

import java.io.IOException;
import static org.junit.jupiter.api.Assertions.*;

public class ConnectionTest {

    Client client;
    Client client2;
    UnoServer server;

    @BeforeEach
    void setup() throws IOException {
        server = new UnoServer();
        new Thread(server).start();

        client = new Client();
        client2 = new Client();
        new Thread(client.getServerConn()).start();
        new Thread(client2.getServerConn()).start();
    }

    @Test
    void testIncomingMessage() throws IOException, InterruptedException {
        client.sendTestMessage();
        client2.sendTestMessage();
        Thread.sleep(1000);
        assertEquals("MSGB Welcome Test1 S ", client.getServerConn().getLatestMessage());
    }

    @Test
    void testClientHandlers() {
        assertEquals(server.handlerList.get(0).getName(), client.getName());
        assertEquals(server.handlerList.get(1).getName(), client2.getName());
    }





}
