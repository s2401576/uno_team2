package Model.Server_Client;

import Controller.InputHandler;
import Model.Exceptions.UnknownMessageException;
import Model.Structure.Card;
import Model.Structure.Game;
import Model.Players.Player;
import Protocol.Protocol;

import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

public class Client implements Protocol, Player {

    private static final String serverIP = "localhost";
    private static final int port = 2008;

    private Socket socket;
    private InputHandler serverConn;
    private BufferedReader keyboard;
    private BufferedWriter out;
    private String name;
    public static String lastQuestion;
    private int score;
    private boolean human;
    private Game game;


    public Client() throws IOException{
        socket = new Socket(serverIP, port);
        serverConn = new InputHandler(socket, this);
        keyboard = new BufferedReader(new InputStreamReader(System.in));
        out = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
        //this.name = name;
    }

    public InputHandler getServerConn() {
        return serverConn;
    }


    /**
     * Handles the commands that are typed by the user.
     * This is used in combination with the protocol methods so the messages that are sent
     * are structured in such a way that they can be interpreted by the handler method of the receiver.
     * @param command the command that is typed by the user
     * @return a String object that follows the protocol in terms of structure
     */
    public String commandHandler(String command) throws UnknownMessageException {

        String[] words = command.split(Protocol.SEPERATOR);

        if (isInteger(command)){
            return MyMove(Integer.parseInt(command));
        }

        if (words[0].equalsIgnoreCase("announce")){
            System.out.println("handling announce");
            this.human = true;  //assumption is made that this is a human player
            if (words.length > 3){
                if (words[3].equalsIgnoreCase("computer")){
                    this.human = false; //human assumption can be overruled
                }
                return "MSGB Announce " +  words[1] + Protocol.SEPERATOR + findFlag(words[2]) + Protocol.SEPERATOR + words[3];
            }
            return AnnounceMsg(words[1], findFlag(words[2]));
        }
        if (words[0].equalsIgnoreCase("can") && words[1].equalsIgnoreCase("we") && words[2].equals("start?")){
            return CanWeStart();
        }
        if (words[0].equalsIgnoreCase("yes")){
            return CompleteMsg("Yes");
        }

        //throw new UnknownMessageException("Input is not recognized.");
        return "Not a valid command";

    }

    /**
     * checks if the given String object represents an integer
     * @param str the message that the user has typed and that is checked
     * @return true if this is a String representation of an Integer
     */
    public boolean isInteger(String str){
        for (int i = 0; i < 99; i++){
            if (Integer.toString(i).equals(str)){
                return true;
            }

        }
        return false;
    }

    public boolean isHuman(){
        return this.human;
    }

    public void sendTestMessage() throws IOException {
        out.write(AnnounceMsg("Test1", Flags.S));
        out.newLine();
        out.flush();
    }

    public static void main(String[] args) throws IOException, UnknownMessageException {


        //System.out.println("Give a name for this client");
        Client tc = new Client();
        //new BufferedReader(new InputStreamReader(System.in)).readLine()


        new Thread(tc.serverConn).start();


        while(true) {
            System.out.println("> ");
            String command = tc.keyboard.readLine();

            if (command.equalsIgnoreCase("quit")) break;
            String message = tc.commandHandler(command);

            //System.out.println("I am going to send: " + message);

            tc.out.write(message);
            tc.out.newLine();
            tc.out.flush();

//        if (tc.human){
//
//            }
//        }else{
//            while(true){
//
//            }
//        }
        }
            tc.socket.close();
            System.exit(0);

    }


    public void respond(String serverMsg){
        String[] words = serverConn.getLatestMessage().split(Protocol.SEPERATOR);

        switch (words[1]){
            case "YTurn":
                makeMove();
                break;
            case "ChooseColor":
                chooseColor();
                break;
        }

    }


    //////PROTOCOL METHODS///////
    @Override
    public String CompleteMsg(String messageContents) {
        return String.format(MSGBEGIN + SEPERATOR + messageContents + SEPERATOR);
    }

    @Override
    public String StructCard(Card card) {
        return null;
    }

    @Override
    public String DisplayError(ErrorTypes error) {
        return null;
    }

    @Override
    public String AnnounceMsg(String name, Flags flag) {
        return CompleteMsg(String.format("Announce"+SEPERATOR+name+SEPERATOR+flag.name()));
    }

    @Override
    public String CanWeStart() {
        return CompleteMsg("CanWeStart");
    }

    @Override
    public String ConfStart(boolean confirm) {
        return null;
    }

    @Override
    public String MyMove(Integer index) {
        return CompleteMsg(index.toString());    }

    @Override
    public String ConfRestart(boolean confirm) {
        return null;
    }

    @Override
    public String disconnect(String playerName) {
        return null;
    }

    @Override
    public String WelcomeMsg(String name, Flags flag) {
        return null;
    }

    @Override
    public String StartQBC() {
        return null;
    }

    @Override
    public String StartBC(boolean starting, Card card, List<String> playersList) {
        return null;
    }

    @Override
    public String TurnBC(String namePlayer, Card card) {
        return null;
    }

    @Override
    public String YourTurn(String namePlayer, List<Card> cardsList) {
        return null;
    }

    @Override
    public String MoveBC(String namePlayer, boolean playCard, Card card) {
        return null;
    }

    @Override
    public String ConsequenceBC(String namePlayer, String colourCard, String valueCard) {
        return null;
    }

    @Override
    public String HasUnoBC(String namePlayer) {
        return null;
    }

    @Override
    public String RoundWinnerBC(String namePlayer, boolean hasWinner, int pointsAdded) {
        return null;
    }

    @Override
    public String PointsOverview(List<String> playersList, List<Integer> pointsList) {
        return null;
    }

    @Override
    public String NxtRoundQBC() {
        return null;
    }

    @Override
    public String IsFinalWinnerBC(String namePlayer) {
        return null;
    }

    @Override
    public String RestartQBC() {
        return null;
    }

    @Override
    public String chooseColor() {
        return CompleteMsg("ChooseColor");
    }

    public Flags findFlag(String str){
        switch (str.toUpperCase(Locale.ROOT)){
            case "S":
                return Flags.S;
            case "P":
                return Flags.P;
        }
        return null;
    }

    @Override
    public void placeCard(Card card) {

    }

    @Override
    public void takeCard() {

    }

    @Override
    public void chooseColour(int colour) {

    }

    @Override
    public int getScore() {
        return 0;
    }

    @Override
    public void setScore(int newScore) {
        this.score = newScore;
    }

    @Override
    public ArrayList<Card> getDeck() {
        return null;
    }

    @Override
    public String getName() {
        return null;
    }

    @Override
    public void addToDeck(Card card) {

    }

    @Override
    public void makeMove() {
        ArrayList<Card> playable = new ArrayList<>();
            for (Card c : game.getCurrentPlayerObject().getDeck()) {
                if (game.isValid(c)) {
                    playable.add(c);
                }

            }
            if (playable.size() == 0) {
                System.out.println("taking card");
                takeCard();
                playable.clear();
            } else {
                Collections.shuffle(playable);
                placeCard(playable.get(0));
            }

    }

    @Override
    public void setGame(Game game) {
        this.game = game;
    }

    @Override
    public ArrayList<Card> getPlayable() {
        return null;
    }

    @Override
    public Card getCardByInt(Integer i) {
        return null;
    }
}
