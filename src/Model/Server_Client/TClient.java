//package Client;
//
//
//import Protocol.Protocol;
//import Protocol.ProtocolClass;
//
//import java.io.*;
//import java.net.Socket;
//import java.net.UnknownHostException;
//import java.util.List;
//import java.util.Locale;
//
//public class Client implements Protocol {
//    public static final int listening_port = 2008;
//    private String hostName = "localhost";
//    private Socket clientSocket;
//    private BufferedReader in;
//    private BufferedWriter out;
//    private BufferedReader userInput;
//
//    Run connection;
//
//
//    public void setupConnection(){
//        try {
//            System.out.println("Setting up connection...");
//            clientSocket = new Socket(hostName, listening_port);
//            in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
//            out = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream()));
//            userInput = new BufferedReader(new InputStreamReader(System.in));
//            connection = new Run(clientSocket);
//
//        } catch (IOException e){
//            e.printStackTrace();
//        }
//    }
//
//    public void closeConnection(){
//        System.out.println("Closing the connection...");
//        try {
//            in.close();
//            out.close();
//            clientSocket.close();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }
//
//    public boolean announce() throws IOException {
//        System.out.println("Hello client, what is your name?");
//        String[] msg = userInput.readLine().split(Protocol.SEPERATOR);
//        out.write(AnnounceMsg(msg[0], Flags.valueOf(msg[1].toUpperCase(Locale.ROOT))));
//        out.newLine();
//        out.flush();
//        System.out.println("announced");
//        return true;
////        System.out.println(in.readLine());
////        System.out.println("?");
//    }
//
//
//    public void go() {
//            try {
//                while(true) {
//                    System.out.println("checking");
//                    String incomingMsg = in.readLine();
//                    if (incomingMsg != null){
//                        System.out.println(incomingMsg);
//                        String response = createResponse(incomingMsg);
//                        out.write(createResponse(incomingMsg));
//                        out.flush();
//                    }
//                }
//            } catch (IOException e) {
//                System.out.println(e.getMessage());
//            }
//
//    }
//
//    public String createResponse(String incomingMsg) throws IOException {
//        if (incomingMsg.equals("Ready?")){
//            return userInput.readLine();
//        }
//        return null;
//    }
//
//    public static void main(String[] args) throws IOException {
//        Client c = new Client();
//        c.setupConnection();
//        c.announce();
//        System.out.println(c.readLineFromServer());
//        while (true){
//            System.out.println("Server says: " + c.readLineFromServer());
//            c.sendMessage(c.userInput.readLine());
//        }
//    }
//
//
//    @Override
//    public String CompleteMsg(String messageContents) {
//        return String.format(MSGBEGIN + SEPERATOR + messageContents + SEPERATOR);
//    }
//
//    @Override
//    public String StructCard(String colour, String value) {
//        return null;
//    }
//
//    @Override
//    public String DisplayError(ErrorTypes error) {
//        return null;
//    }
//
//    @Override
//    public String AnnounceMsg(String name, Flags flag) {
//        return CompleteMsg(String.format("Announce"+SEPERATOR+name+SEPERATOR+flag.name()));
//    }
//
//    @Override
//    public String CanWeStart() {
//        return null;
//    }
//
//    @Override
//    public String ConfStart(boolean confirm) {
//        return null;
//    }
//
//    @Override
//    public String MyMove(boolean playCard, String colour, String value) {
//        return null;
//    }
//
//    @Override
//    public String ConfRestart(boolean confirm) {
//        return null;
//    }
//
//    @Override
//    public String disconnect(String playerName) {
//        return null;
//    }
//
//    @Override
//    public String WelcomeMsg(String name, Flags flag) {
//        return null;
//    }
//
//    @Override
//    public String StartQBC() {
//        return null;
//    }
//
//    @Override
//    public String StartBC(boolean starting, String colourTopCard, String valueTopCard, List<String> playersList) {
//        return null;
//    }
//
//    @Override
//    public String TurnBC(String namePlayer, String colourTopCard, String valueTopCard) {
//        return null;
//    }
//
//    @Override
//    public String YourTurn(String namePlayer, List<String> cardsList) {
//        return null;
//    }
//
//    @Override
//    public String MoveBC(String namePlayer, boolean playCard, String colourCard, String valueCard) {
//        return null;
//    }
//
//    @Override
//    public String ConsequenceBC(String namePlayer, String colourCard, String valueCard) {
//        return null;
//    }
//
//    @Override
//    public String HasUnoBC(String namePlayer) {
//        return null;
//    }
//
//    @Override
//    public String RoundWinnerBC(String namePlayer, boolean hasWinner, int pointsAdded) {
//        return null;
//    }
//
//    @Override
//    public String PointsOverview(List<String> playersList, List<Integer> pointsList) {
//        return null;
//    }
//
//    @Override
//    public String NxtRoundQBC() {
//        return null;
//    }
//
//    @Override
//    public String IsFinalWinnerBC(String namePlayer) {
//        return null;
//    }
//
//    @Override
//    public String RestartQBC() {
//        return null;
//    }
//
//    public synchronized void sendMessage(String msg)
//              {
//        if (out != null) {
//            try {
//                out.write(msg);
//                out.newLine();
//                out.flush();
//            } catch (IOException e) {
//                System.out.println(e.getMessage());
//                System.out.println("ioexeptneoienoinoienf");
//            }
//        }
//    }
//
//    public String readLineFromServer() {
//        if (in != null) {
//            try {
//                // Read and return answer from Server
//                String answer = in.readLine();
//                if (answer == null) {
//                    System.out.println("nothing can be read");
//                }
//                return answer;
//            } catch (IOException e) {
//                return "IOException";
//            }
//        } else {
//            return "error";
//        }
//    }
//}
