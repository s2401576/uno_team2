package Model.Server_Client;

import Controller.ClientHandler;
import Model.Structure.Card;
import Model.Structure.Game;
import Model.Players.Player;
import Protocol.Protocol;
import View.ServerTUI;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class UnoServer implements Runnable, Protocol {

    private ServerSocket ssock;
    public static final int portNumber = 2008;
    public ArrayList<ClientHandler> handlerList;
    private boolean ready;
    public Game game;
    private ClientHandler currentHandler;
    public int wantsToStart = 0;
    public ServerTUI view;
    public String lastMessage;
    private ArrayList<Player> serverPList;
    private Flags desiredFlag;

    public UnoServer(){
        this.handlerList = new ArrayList<>();
        this.serverPList = new ArrayList<>();
    }

    public ArrayList<Player> getServerPList(){
        return this.serverPList;
    }

    public void addToPlayerList(Player player){
        this.serverPList.add(player);
    }

    public void run(){
        //initialize a new ServerSocket
        try {
            ssock = new ServerSocket(portNumber);
        } catch (IOException e){
            System.out.println(e.getMessage());
        }
        //keep looking for clients that want to connect and accept them
        while (true){
            try{
                Socket clientSocket = ssock.accept();
                ClientHandler c = new ClientHandler(clientSocket, this);
                new Thread(c).start();
                handlerList.add(c);
                System.out.println("clientHandler ready");
            } catch (IOException e){
                System.out.println(e.getMessage());
            }
        }
    }

    public void removeClient(ClientHandler c){
        handlerList.remove(c);
    }

    public static void main(String[] args){
        UnoServer server = new UnoServer();
        server.view = new ServerTUI(server);
        System.out.println("trying to start server...");
        new Thread(server).start();
        System.out.println("Server started");
    }

    //gathers players and creates a game object
    public void startGame(){
        //ArrayList<Player> players = new ArrayList<>();
//        for (ClientHandler c : handlerList){
//            players.add(new HumanPlayer(c.getName()));
//        }

        Game game = new Game(serverPList);
        this.game = game;
        if (desiredFlag == Flags.P){
            game.normal = false;
            game.progressive = true;
        } else{
            game.normal = true;
            game.progressive = false;
        }
        for (Player p : game.getPlayerList()){
            p.setGame(game);
            game.setPlayerMap(p);
        }

        game.dealCardsAndShuffle(game.getDrawPile(), game.getPlayerList());

        currentHandler = handlerList.get(game.getCurrentPlayer());
        currentHandler.sendMessage(this.YourTurn(this.game.getPlayerList().get(this.game.getCurrentPlayer()).getName(), game.getPlayerList().get(this.game.getCurrentPlayer()).getDeck()));
        ;
        //game.play();
    }



    public void nextPlayer(){
//        if (game.getDirection() == Game.Direction.LEFT){
//            if (currentHandler.equals(handlerList.get(0))){
//                currentHandler = handlerList.get(handlerList.size()-1);
//            } else {
//               currentHandler = handlerList.get(handlerList.indexOf(currentHandler)-1);
//            }
//        }
//        if (game.getDirection() == Game.Direction.RIGHT){
//            if (currentHandler.equals(handlerList.get(handlerList.size()-1))){
//                currentHandler = handlerList.get(0);
//            } else {
//                currentHandler = handlerList.get(handlerList.indexOf(currentHandler)+1);
//            }
//        }
        currentHandler = handlerList.get(game.getCurrentPlayer());
        currentHandler.sendMessage(YourTurn(game.getCurrentPlayerObject().getName(), game.getCurrentPlayerObject().getDeck()));
        currentHandler.sendMessage(game.getCurrentPlayerObject().getName() + ", this is your deck: " + view.getPlayerDeckString(game.getCurrentPlayerObject().getDeck()));

    }



    public void startPlaying(Game game){

//        while (!game.isOver()){
//            game.decideTurn(game.getRecentCard());
//
//            System.out.println("Player " + game.getPlayerMap().get(game.getCurrentPlayer()).getName() + " is now playing");
//
//            //view.printDeck(game.getPlayerMap().get(game.getCurrentPlayer()));
//
//            game.getPlayerMap().get(game.getCurrentPlayer()).makeMove();
//
//            //System.out.println("New card on top: " + view.cardString(game.getRecentCard()));
//
//            if (game.getRecentCard().getSpecial() == Card.Special.TAKE_TWO || game.getRecentCard().getSpecial() == Card.Special.TAKE_FOUR) {
//                game.DrawCards(game.getRecentCard());
//            }
//
//
//            System.out.println("Updated deck from player: ");
//            //view.printDeck(game.getPlayerMap().get(game.getCurrentPlayer()));
//
//
//            game.isOver();
////            if (game.isOver()) {
////                game.calculateScore();
////                System.out.println("Player score is: " + game.getPlayerMap().get(game.getCurrentPlayer()).getScore());
////            }
//        }

    }

    //when this method is called, it means that one player does not have any cards left.
    public void finalizeGame(Game game){
        for (Player p : game.getPlayerList()){
            if (p.getDeck().isEmpty()){ //means player p has won the game
                game.calculateScore(p.getScore(), p);
            }
        }

    }

    public Flags getDesiredFlag(){
        return this.desiredFlag;
    }

    public void setDesiredFlag(Flags desiredFlag) {
        this.desiredFlag = desiredFlag;
    }



    public boolean ready(){
        return wantsToStart == handlerList.size();
    }


    @Override
    public String CompleteMsg(String messageContents) {
        return String.format(MSGBEGIN + SEPERATOR + messageContents + SEPERATOR);
    }

    @Override
    public String StructCard(Card card) {
        String firstPart = null;
        String secondPart = null;
        String colour = card.getColor().toString();
        String value = card.getSpecial().toString();
        int number = card.getValue();
        switch(colour){
            case "RED":
                firstPart = "R";
                break;
            case "BLUE":
                firstPart = "B";
                break;
            case "YELLOW":
                firstPart = "Y";
                break;
            case "GREEN":
                firstPart = "G";
                break;
            case "WILD":
                firstPart = "S";
                break;
            default:
                //print error
                break;
        }
        switch (value){
            case "REVERSE":
                secondPart = "R";
                break;
            case "SKIP":
                secondPart = "S";
                break;
            case "TAKE_TWO":
                secondPart = "D";
                break;
            case "COLOR":
                secondPart = "1";
                break;
            case "TAKE_FOUR":
                secondPart = "2";
                break;
//            case "NONE":
//                secondPart = null;
//                break;
            default:
                try {
                    if (number >= 0 && number < 10) {
                        secondPart = Integer.toString(number);
                    }
                    if (number >= 20 && number <= 50) {
                        break;
                    }
                } catch(NumberFormatException e){
                    DisplayError(ErrorTypes.E2);
                }
                break;
        }
        return (firstPart+secondPart);
    }

    @Override
    public String DisplayError(ErrorTypes error) {
        return null;
    }

    @Override
    public String AnnounceMsg(String name, Flags flag) {
        return null;
    }

    @Override
    public String CanWeStart() {
        return CompleteMsg("CanWeStart");
    }

    @Override
    public String ConfStart(boolean confirm) {
        return null;
    }

    @Override
    public String MyMove(Integer index) {
        return CompleteMsg(index.toString());
    }

    @Override
    public String ConfRestart(boolean confirm) {
        return null;
    }

    @Override
    public String disconnect(String playerName) {
        return null;
    }

    @Override
    public String WelcomeMsg(String name, Flags flag) {
        return CompleteMsg("Welcome"+SEPERATOR+name+SEPERATOR+flag.name());
    }

    @Override
    public String StartQBC() {
        return CompleteMsg("Start?");
    }

    @Override
    public String StartBC(boolean starting, Card card, List<String> playersList) {
        String allNames = "";
        for (String s : playersList) {
            allNames = String.format(allNames + SEPERATOR + s);
        }
        return CompleteMsg(String.format("Starting:"+SEPERATOR+starting+SEPERATOR+StructCard(card)+SEPERATOR+allNames));
    }

    @Override
    public String TurnBC(String namePlayer, Card card) {
        return null;
    }

    @Override
    public String YourTurn(String namePlayer, List<Card> cardsList) {
        String totalCards = "";
        for(int i = 0; i < cardsList.size(); i++){
            totalCards = String.format(totalCards + SEPERATOR + StructCard(cardsList.get(i)));
        }

        return CompleteMsg(String.format("YTurn"+SEPERATOR+namePlayer+SEPERATOR+totalCards));    }

    @Override
    public String MoveBC(String namePlayer, boolean playCard, Card card) {
        String moveBC = null;
        if(playCard){
            moveBC = String.format("MoveBC"+SEPERATOR+true+SEPERATOR+StructCard(card));
        }
        else{
            moveBC = String.format("MoveBC"+SEPERATOR+true+SEPERATOR+null);
        }
        return CompleteMsg(moveBC);
    }

    @Override
    public String ConsequenceBC(String namePlayer, String colourCard, String valueCard) {
        return null;
    }

    @Override
    public String HasUnoBC(String namePlayer) {
        return null;
    }

    @Override
    public String RoundWinnerBC(String namePlayer, boolean hasWinner, int pointsAdded) {
        String roundWinnerBC = null;
        if(hasWinner){
            roundWinnerBC = String.format("RoundWinner"+SEPERATOR+true+SEPERATOR+namePlayer+SEPERATOR+pointsAdded);
        }
        else{
            roundWinnerBC = String.format("RoundWinner"+SEPERATOR+true);
        }
        return CompleteMsg(roundWinnerBC);
    }

    @Override
    public String PointsOverview(List<String> playersList, List<Integer> pointsList) {
        return null;
    }

    @Override
    public String NxtRoundQBC() {
        return null;
    }

    @Override
    public String IsFinalWinnerBC(String namePlayer) {
        return null;
    }

    @Override
    public String RestartQBC() {
        return null;
    }

    @Override
    public String chooseColor(){
        return CompleteMsg("ChooseColor");
    }
}
