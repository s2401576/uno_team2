package Model.Structure;

import java.util.ArrayList;

public class Card {
    public enum Color{
        BLUE, YELLOW, RED, GREEN, WILD
    }

    public enum Special {
        TAKE_TWO, TAKE_FOUR, COLOR, REVERSE, SKIP, NONE
    }

    private Color color;
    private Special special;
    int value;

    ArrayList<Card> deck;

    public Card(Color color, Special special, int value){
        this.color = color;
        this.special = special;
        this.value = value;
    }


    /**
     * @return the color of this card object
     */
    public Color getColor(){
        return this.color;
    }


    /**
     * choose this card to have a specific color
     * @param color the chosen color
     */
    public void setColorrandom(int color) {
        this.color = Color.values()[color];
    }

    public void setColor(Color color) {
        this.color = color;
    }

    /**
     * @return the special type of this card object
     */
    public Special getSpecial(){
        return this.special;
    }

    /**
     * @return the value of this card object
     */
    public int getValue(){
        return this.value;
    }


}
