package Model.Structure;

import Model.Players.Player;
import Model.Structure.Card;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

public class Game {

    public static ArrayList<Integer> UnoDrawPile= new ArrayList<>();
    HashMap<Integer, Player> playerMap; //contains the players in this game with their respective number
    private ArrayList<Player> playerList;
    public int currentPlayer; //the number of the player that currently has the turn, first player is player 0.
    private ArrayList<Card> discardPile;
    private ArrayList<Card> drawPile;



    enum Direction{
        LEFT, RIGHT //direction in which the order of turns is going
    }
    private Direction direction;
    private Card mostRecentCard;

    public boolean progressive = false;
    public boolean normal = true;


    public Game(ArrayList<Player> players){
        this.playerList = players;
        this.drawPile = generatePile();
        this.currentPlayer = 0;
        this.direction = Direction.RIGHT;
        this.drawPile = generatePile();
        this.playerMap = new HashMap<>();
        this.discardPile = new ArrayList<>();
        this.mostRecentCard = new Card(Card.Color.RED, Card.Special.NONE, 4);
    }


    /**
     * @return the Player object of the player that is allowed to make a play
     */
    public Player getCurrentPlayerObject(){
        return playerList.get(this.getCurrentPlayer());
    }

    /**
     * generatePile is used to create a list full of card objects with the correct properties
     * @return list of cards that includes all cards in a UNO game
     */
    public static ArrayList<Card> generatePile(){
        ArrayList<Card> resultDeck = new ArrayList<>();
        int i = 0;  //starts with number 0
        while(i <= 9){  //goes through all the numbers
            if (i == 0){
                resultDeck.add(new Card(Card.Color.RED, Card.Special.NONE, i));   //adds a card of every color given the number
            } else {
                for (int p = 0; p < 2; p++) {
                    resultDeck.add(new Card(Card.Color.RED, Card.Special.NONE, i));   //adds a card of every color given the number twice
                }
            }
            i++;
        }

        i = i-10;
        while(i <= 9){  //goes through all the numbers
            if (i == 0){
                resultDeck.add(new Card(Card.Color.BLUE, Card.Special.NONE, i));   //adds a card of every color given the number
            } else {
                for (int p = 0; p < 2; p++) {
                    resultDeck.add(new Card(Card.Color.BLUE, Card.Special.NONE, i));   //adds a card of every color given the number twice
                }
            }
            i++;
        }

        i = i-10;
        while(i <= 9){  //goes through all the numbers
            if (i == 0){
                resultDeck.add(new Card(Card.Color.GREEN, Card.Special.NONE, i));   //adds a card of every color given the number
            } else {
                for (int p = 0; p < 2; p++) {
                    resultDeck.add(new Card(Card.Color.GREEN, Card.Special.NONE, i));   //adds a card of every color given the number twice
                }
            }
            i++;
        }
        i = i-10;

        while(i <= 9){  //goes through all the numbers
            if (i == 0){
                resultDeck.add(new Card(Card.Color.YELLOW, Card.Special.NONE, i));   //adds a card of every color given the number
            } else {
                for (int p = 0; p < 2; p++) {
                    resultDeck.add(new Card(Card.Color.YELLOW, Card.Special.NONE, i));   //adds a card of every color given the number twice
                }
            }
            i++;
        }

        for (int x = 0; x < 2; x++){    //add two times
            resultDeck.add(new Card(Card.Color.RED, Card.Special.TAKE_TWO, 20));   //+2 card
            resultDeck.add(new Card(Card.Color.RED, Card.Special.REVERSE, 20));    //reverse
            resultDeck.add(new Card(Card.Color.RED, Card.Special.SKIP, 20));//skip a players turn

        }for (int x = 0; x < 2; x++){    //add two times
            resultDeck.add(new Card(Card.Color.BLUE, Card.Special.TAKE_TWO, 20));   //+2 card
            resultDeck.add(new Card(Card.Color.BLUE, Card.Special.REVERSE, 20));    //reverse
            resultDeck.add(new Card(Card.Color.BLUE, Card.Special.SKIP, 20));       //skip a players turn

        }for (int x = 0; x < 2; x++){    //add two times
            resultDeck.add(new Card(Card.Color.GREEN, Card.Special.TAKE_TWO, 20));   //+2 card
            resultDeck.add(new Card(Card.Color.GREEN, Card.Special.REVERSE, 20));    //reverse
            resultDeck.add(new Card(Card.Color.GREEN, Card.Special.SKIP, 20));       //skip a players turn
        }

        for (int x = 0; x < 2; x++){    //add two times
            resultDeck.add(new Card(Card.Color.YELLOW, Card.Special.TAKE_TWO, 20));   //+2 card
            resultDeck.add(new Card(Card.Color.YELLOW, Card.Special.REVERSE, 20));    //reverse
            resultDeck.add(new Card(Card.Color.YELLOW, Card.Special.SKIP, 20));       //skip a players turn
        }

        for (int y = 0; y < 4; y++){
            resultDeck.add(new Card(Card.Color.WILD, Card.Special.COLOR, 50));  //change the color
            resultDeck.add(new Card(Card.Color.WILD, Card.Special.TAKE_FOUR, 50)); //+4 card
        }

        return resultDeck;

    }   //generates a list with all the cards in the game


    /**
     * shuffles the cards and gives 7 to each player in the game
     * @param cards the deck of cards to be dealt
     * @param players player object that receives cards
     */
    public void dealCardsAndShuffle(ArrayList<Card> cards, ArrayList<Player> players){
        Collections.shuffle(cards); //the generated deck is shuffled
        for (Player p: playerList){
            for (int i = 0; i < 7; i++){ //amount of cards
                p.addToDeck(takeOneCard());
            }
        }
    } //shuffles the cards and then assigns cards to each player's deck


    /**
     * decides which player has the turn based on the last played card
     * @param playedCard the card that was just played
     */
    public void decideTurn(Card playedCard){
        if (playedCard.getSpecial() == Card.Special.REVERSE){    //check if card is reverser and change direction accordingly
            if (direction == Direction.LEFT){
                direction = Direction.RIGHT;
            } else {
                direction = Direction.LEFT;
            }
        }

        if (playedCard.getSpecial() == Card.Special.SKIP) { //check if card is a skip and skip player accordingly
            skip(); //refer to skip function
            return;
        }

        //after configuring the direction, adjust the current player who receives the turn
        if (direction == Direction.RIGHT){
            if (currentPlayer == playerList.size()-1){
                currentPlayer = 0;
            } else {
                currentPlayer++;
            }
        }
        if (direction == Direction.LEFT){
            if (currentPlayer == 0){
                currentPlayer = playerList.size()-1;
            } else {
                currentPlayer--;
            }
        }
    }

    /**
     * this method is used to skip one player
     */
    public void skip() { //function for skipping a player
        if (playerList.size() == 2) { //if there are only two players the same player simply goes again
            return;
        }
        if (direction == Direction.RIGHT){
            if (currentPlayer == playerList.size()-1){
                currentPlayer = 1;
                return;
            } if (currentPlayer == playerList.size()-2) {
                currentPlayer = 0;
                return;
            }
            else {
                currentPlayer++;
                currentPlayer++;
                return;
            }
        }

        if (direction == Direction.LEFT){
            if (currentPlayer == 0){
                currentPlayer = playerList.size()-1;
                currentPlayer--;
                return;
            } if (currentPlayer == 1) {
                currentPlayer = playerList.size()-1;
            }
            else {
                currentPlayer--;
                currentPlayer--;
            }
        }
    }

    /**
     * this method is used to reshuffle the cards
     */
    public void reshuffle() {
        Collections.shuffle(getDiscardPile());
        getDrawPile().addAll(getDiscardPile());
        discardPile.clear();
    }


    // function for the take_two and take_four cards

    /**
     * method makes the next player take cards after a +4 or +2 card is played
     * @param card the played card which is checked for being a +4 or +2
     */
    public void DrawCards(Card card) {
        if (card.getSpecial() == Card.Special.TAKE_TWO) {
            if (direction == Direction.RIGHT) {
                if (currentPlayer == playerList.size()-1) {
                    takeOneCardPlayer(getPlayerMap().get(0)); //get two cards from draw pile for next player to the right at end of list
                    takeOneCardPlayer(getPlayerMap().get(0));

                } else {
                    takeOneCardPlayer(getPlayerMap().get(getCurrentPlayer()+1)); //get two cards from draw pile for next player to the right
                    takeOneCardPlayer(getPlayerMap().get(getCurrentPlayer()+1));
                }

            }
            if (direction == Direction.LEFT) {
                if (currentPlayer == 0) {
                    takeOneCardPlayer(getPlayerMap().get(playerList.size()-1)); //get two cards from draw pile for next player to the left at end of list
                    takeOneCardPlayer(getPlayerMap().get(playerList.size()-1));
                } else {
                    takeOneCardPlayer(getPlayerMap().get(getCurrentPlayer()-1)); //get two cards from draw pile for next player to the left
                    takeOneCardPlayer(getPlayerMap().get(getCurrentPlayer()-1));
                }

            }
        }
        if (card.getSpecial() == Card.Special.TAKE_FOUR) {
            if (direction == Direction.RIGHT) {
                if (currentPlayer == playerList.size()-1) {
                    for (int i =0; i < 4; i++) {
                        takeOneCardPlayer(getPlayerMap().get(0)); //get four cards from draw pile for next player to the right at end of list
                    }
                } else {
                    for (int i =0; i < 4; i++) {
                        takeOneCardPlayer(getPlayerMap().get(getCurrentPlayer()+1)); //get four cards from draw pile for next player to the right
                    }
                }

            }
            if (direction == Direction.LEFT) {
                if (currentPlayer == 0) {
                    for (int i =0; i < 4; i++) {
                        takeOneCardPlayer(getPlayerMap().get(playerList.size()-1)); //get four cards from draw pile for next player to the left when at the end of list

                    }
                } else {
                    for (int i =0; i < 4; i++) {
                        takeOneCardPlayer(getPlayerMap().get(getCurrentPlayer()-1)); //get four cards from draw pile for next player to the left
                    }
                }
            }
        }
    }


    /**
     * take one card from the drawpile
     * @return the taken card
     */
    public Card takeOneCard(){
        if (drawPile.size() == 0) {
            reshuffle();
        }
        Card result = drawPile.get(drawPile.size()-1);
        drawPile.remove(result);
        return result;
    }

    /**
     * assigns a card to a specific player
     * @param player the player who should receive the card
     */
    public void takeOneCardPlayer(Player player){
        if (drawPile.size() == 0) {
            reshuffle();
        }
        Card result = drawPile.get(drawPile.size()-1);
        player.addToDeck(result);
        drawPile.remove(result);
        System.out.println("chosen card: " + result.getColor() + "_" + result.getValue());
    }

    /**
     * test if the played card is a valid more or not
     * @param card the card that is played by the current player
     * @return true if the card is valid, otherwise false
     */
    public boolean isValid(Card card){ //check whether a move is valid including special cards
        if (progressive) { //
            if ((card.getSpecial() == Card.Special.TAKE_TWO && mostRecentCard.getSpecial() == Card.Special.TAKE_TWO) ||
                    (card.getSpecial() == Card.Special.TAKE_FOUR && mostRecentCard.getSpecial() == Card.Special.TAKE_FOUR)) {
                return true;       // take four and take two can be stacked in progressive
            }
            if ((card.getSpecial() == Card.Special.TAKE_FOUR && mostRecentCard.getSpecial() == Card.Special.TAKE_TWO) ||
                    (card.getSpecial() == Card.Special.TAKE_TWO && mostRecentCard.getSpecial() == Card.Special.TAKE_FOUR)) {
                return false;
            }
        }
        if (mostRecentCard.getSpecial() == Card.Special.COLOR || mostRecentCard.getSpecial() == Card.Special.TAKE_FOUR) {
            return mostRecentCard.getColor() == card.getColor() && card.getSpecial() != Card.Special.TAKE_FOUR ||
                    card.getSpecial() == Card.Special.COLOR;
        } // when choosing a colour you can only use the color which was chosen before, not the number (except for another color card)
        if (((card.getSpecial() == Card.Special.TAKE_FOUR && mostRecentCard.getSpecial() != Card.Special.TAKE_FOUR) || card.getSpecial() == Card.Special.COLOR)) {
            return true; // take four and wild colour can always be played except take four on take four
        } if (card.getSpecial() == mostRecentCard.getSpecial() && card.getSpecial() != Card.Special.TAKE_TWO //the same special cards can be stacked
                 && card.getSpecial() != Card.Special.NONE) {
            System.out.println("test");
            return true;
        }
        else {
            return mostRecentCard.getColor() == card.getColor() && card.getSpecial() != Card.Special.TAKE_TWO || (mostRecentCard.getValue() == card.getValue()
                    && card.getSpecial() == Card.Special.NONE && card.getSpecial() != Card.Special.TAKE_TWO);
        }
    }

    /**
     * quick check if the game is over
     * @return true if one of the players does not have any cards in their hand
     */
    public boolean isOver(){ //check whether the game is over
        for (Player p : playerList){
            if(p.getDeck().isEmpty()){
                return true;
            }
        }
        return false;
    }

    /**
     * calculates and sets the score for a player given their current score
     * @param score the current score of the player
     * @param player the player whose score should be updated
     */
    public void calculateScore(int score, Player player) {
        for (Player p : playerList) {
            for (int i = 0; i < p.getDeck().size(); i++) {
                score += p.getDeck().get(i).getValue();
            }
        }
        player.setScore(score);
    }

    /**
     * calculates the score a given player scored in the game
     * @param player the player for who the score should be calculated
     * @return the scored points
     */
    public int gameScore(Player player){
        int score = 0;
        for (Player p : playerList) {
            for (int i = 0; i < p.getDeck().size(); i++) {
                score += p.getDeck().get(i).getValue();
            }
        }
        return score;
    }

    public void SEVEN_ZERO_RULE(Card card) {
        if (card.getValue() == 0) {
            for (Player p: playerList) {
                for (int i = 0; i < p.getDeck().size(); i++) {

                }
            }
        }
    }


    /**
     * places a card on the discard pile
     * @param card the card to be placed
     */
    public void discard(Card card){
        this.discardPile.add(card);
    }   //place a card on the discard pile (does not include validity check)

    /**
     * gives the card that is most recently added to the pile
     * @return the most recent card
     */
    public Card getRecentCard(){
        return this.mostRecentCard;
    }   //returns the most recent placed card on the discard pile

    public void setMostRecentCard(Card card){
        mostRecentCard = card;
    }
    public void setCardColour(Card.Color colour) {
        mostRecentCard.setColor(colour);
    }

    public ArrayList<Card> getDrawPile(){
        return this.drawPile;
    }

    public ArrayList<Card> getDiscardPile(){
        return this.discardPile;
    }

    public HashMap<Integer, Player> getPlayerMap(){
        return this.playerMap;
    }
    public ArrayList<Player> getPlayerList(){
        return this.playerList;
    }
    public int getCurrentPlayer(){
        return this.currentPlayer;
    }
    public void setCurrenplayer(int currentPlayer){this.currentPlayer = currentPlayer;}

    public void setDirectionright() {
        this.direction = Direction.RIGHT;
    }

    public void setDirectionleft() {
        this.direction = Direction.LEFT;
    }

    public void setPlayerMap(Player p){
        if (playerMap.size() == 0){
            playerMap.put(0, p);
        } else {
            playerMap.put(playerMap.size(), p);
        }
    }
}


//