package Model.Players;

import Model.Structure.Card;
import Model.Structure.Game;

import java.util.ArrayList;

public interface Player {

    void placeCard(Card card) throws InterruptedException;
        //some method to put it on the table
        //this.deck.remove(card); //remove the card from the deck


    void takeCard();
        //one card from the pile should be put in this players deck
        //deck.add((*random item from available cards*))

    void chooseColour(int colour);
    //some method to choose the colour

    int getScore();

    void setScore(int newScore);

    ArrayList<Card> getDeck();

    String getName();

    void addToDeck(Card card);

    void makeMove() throws InterruptedException;

    void setGame(Game game);

    public ArrayList<Card> getPlayable();

    Card getCardByInt(Integer i);


}
