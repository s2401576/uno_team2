package Model.Players;

import Model.Structure.Card;
import Model.Structure.Game;

import java.util.ArrayList;

public class HumanPlayer implements Player {

    private String name;
    private int score;
    private ArrayList<Card> deck;
    Game game;
    private Card takenCard;

    public HumanPlayer(String name){   //create a new HumanPlayer object with 0 score and empty deck
        this.name = name;
        this.score = 0;
        this.deck = new ArrayList<>();
    }

    public Card getCardByInt(Integer i){
        return deck.get(i-1);
    }

    @Override
    public void placeCard(Card card) throws InterruptedException {
        game.discard(card); //adds the card to the discard pile
        game.setMostRecentCard(card);
        this.deck.remove(card); //remove the card from the deck

//        if (card.getSpecial() == Card.Special.COLOR || card.getSpecial() == Card.Special .TAKE_FOUR) {
//            chooseColour(1); //need to make the user choose this
//        }
    }

    @Override
    public void takeCard() {
        if (game.getDrawPile().size() == 0) {
            game.reshuffle();
        }
        deck.add(game.getDrawPile().get(game.getDrawPile().size()-1));
        game.getDrawPile().remove(game.getDrawPile().size()-1);
    }

    @Override
    public void chooseColour(int colour) {
        //game.getRecentCard().setColorrandom(1); //need to make the user decide this value
    }

    @Override
    public int getScore() {
        return this.score;
    }

    @Override
    public void setScore(int newScore) {
        this.score = newScore;
    }

    @Override
    public ArrayList<Card> getDeck() {
        return this.deck;
    }

    public String getName(){
        return this.name;
    }

    public void addToDeck(Card card){
        this.deck.add(card);
    }

    public void makeMove(){

    }

    public void setGame(Game game){
        this.game = game;
    }

    @Override
    public ArrayList<Card> getPlayable() {
        return null;
    }
}
