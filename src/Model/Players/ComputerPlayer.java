package Model.Players;

import Model.Structure.Card;
import Model.Structure.Game;

import java.util.ArrayList;
import java.util.Collections;

public class ComputerPlayer implements Player {

    private String name;
    private int score;
    private ArrayList<Card> deck;
    Game game;
    public ArrayList<Card> playable;

    public ComputerPlayer(String name){
        this.name = name;
        this.score = 0;
        this.deck = new ArrayList<>();
        this.playable = new ArrayList<>();
    }

    public void makeMove() throws InterruptedException {
        if (playable == null) {
            playable = new ArrayList<>();
        } else {
            playable.clear();
            for (Card c : this.deck) {
                if (game.isValid(c)) {
                    playable.add(c);
                }

            }
            if (playable.size() == 0) {
                System.out.println("taking card");
                takeCard();
                playable.clear();
            } else {
                Collections.shuffle(playable);
                placeCard(playable.get(0));
            }
        }
    }





    @Override
    public void placeCard(Card card) {
        game.discard(card); //adds the card to the discard pile
        game.setMostRecentCard(card);
        this.deck.remove(card); //remove the card from the deck

        if (card.getSpecial() == Card.Special.TAKE_FOUR || card.getSpecial() == Card.Special.COLOR) {
            chooseColour((int) (Math.random() * 4));
        }
    }

    @Override
    public void takeCard() {
        game.takeOneCardPlayer(this);
//        deck.add(game.getDrawPile().get(game.getDrawPile().size()-1));
//        game.getDrawPile().remove(game.getDrawPile().size()-1);
    }

    @Override
    public void chooseColour(int colour) {
        game.getRecentCard().setColorrandom(colour); //need to make the user decide this value
        System.out.println("chosen color: " + Card.Color.values()[colour]);
    }
    @Override
    public int getScore() {
        return this.score;
    }

    @Override
    public void setScore(int newScore) {
        this.score = newScore;
    }

    @Override
    public ArrayList<Card> getDeck() {
        return this.deck;
    }

    public String getName(){
        return this.name;
    }

    public void addToDeck(Card card){
        this.deck.add(card);
    }

    public void setGame(Game game){
        this.game = game;
    }

    public ArrayList<Card> getPlayable() {
        return playable;
    }

    @Override
    public Card getCardByInt(Integer i) {
        return null;
    }
}
