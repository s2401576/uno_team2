package Model.Exceptions;

public class InvalidMoveException extends Exception {

    public InvalidMoveException(String msg){
        super(msg);
    }

}
