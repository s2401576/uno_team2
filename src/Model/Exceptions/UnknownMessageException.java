package Model.Exceptions;

public class UnknownMessageException extends Exception{

    public UnknownMessageException(String msg){
        super(msg);
    }


}
