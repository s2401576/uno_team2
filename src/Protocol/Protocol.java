package Protocol;

import Model.Structure.Card;

import java.util.List;

public interface Protocol {
    //Port: 7004



    //START OF GAME
    //Client: Announce "Name" "Flags"
    //Server: BC (Welcome "Name" "Flags")
    //Client: Can we start?
    //Server: BC (Start?)
    //Client: Yes/No/Nothing + Timer 60 seconds
    //Server: BC (NoStart)
    //Server: Start

    //GAME
    //Server: BC(startGame "TopCard" "Names(x2-x8)")
    //----------------MOVE----------------
    //Server: BC (Turn)
    //Server: PlayerTurn "Hand"
    //Client: MyMove "card" / DRAW
    //Server: BC (Move "player" "card")
    //Server: if(BC (Player "consequence"))
    //Server: BC(Player "hasUno")
    //Server: BC(Player "isWinner" "PointsAdded")
    //Server: BC("Player" "points", "Player" "Points", etc.)
    //----------Either option1 or option2-----------
    //Server: (Option 1) BC("isFinalWinner)
    //Server: (Option 2) BC(NextRound) -> Go back to MOVE
    //Server: BC(Restart)
    //Client: Restart yes/no

    //ERRORS
    //List of errors
    //E1 - Invalid move (No correct move, but you do have the card)
    //E2 - Unknown message
    //E3 - Name is taken
    //E4 - Disconnect (client)
    //E5 - Disconnect (Server)(Not possible)
    //E6 - Flag not supported
    //E7 - Unknown Card (Card not in players hand)

    //4. If client disconnects
    //Cards of the player that is disconnected should be moved to the pile and the game is continued
    //If there are only 2 clients playing, the game stops

    //CARDS
    //--Color--
    //RED = R
    //BLUE = B
    //YELLOW = Y
    //GREEN = G
    //SPECIAL = S

    //--Value--
    //NUMBERS = 0,1,2,...,9
    //REVERSE = R
    //SKIP = S
    //DRAW2 = D
    //WildCard = 1
    //Wild+4Card = 2


    ////////////////////////        Parameters          ////////////////////////
    public static final String PROGRESSIVE = "P";
    public static final String SEPERATOR = " ";
    public static final String MSGBEGIN = "MSGB";
    public static enum ErrorTypes{
        E1, E2, E3, E4, E5, E6
    }
    public static enum Flags{
        S, L, C, P
    }

    //////////////////////// Construct complete message ////////////////////////

    /**
     *
     * @param messageContents is the message the client/server wants to send
     * @return Complete String message with a message begin tag and the content
     */
    public String CompleteMsg(String messageContents);

    ////////////////////////    General part messages   ////////////////////////

    /**
     *
     * @param card is the colour of the card
     * @ensures colour is Blue, Red, Green, Yellow, or Special: WildCard (S) or Draw 4 (S)
     * @ensures value is 0-9,R,S or D
     * @return correctly formulated String of the card according to the protocol
     */
    public String StructCard(Card card);

    /**
     *
     * @param error is one of the errors part of the enum ErrorTypes
     * @return correctly formulated error message
     */
    public String DisplayError(ErrorTypes error);

    ////////////////////////    Client part messages    ////////////////////////

    /**
     *
     * @param name is the name of the player that announces his participation
     * @param flag is the version of the game that the player will play
     * @return correctly formulated participation announcement of a player
     */
    public String AnnounceMsg(String name, Flags flag);

    /**
     *
     * @return correctly formulated question to start the game
     */
    public String CanWeStart();

    /**
     *
     * @param confirm confirm == true means player is ready to start, confirm == false means player is not ready yet
     * @return correctly formulated confirmation/deconformation message
     */
    public String ConfStart(boolean confirm);

    /**
     * @return correctly formulated play message of player
     */
    public String MyMove(Integer index);

    /**
     *
     * @param confirm confirm == true means player accepts restart, confirm == false means player doesn't accept restart
     * @return correctly formulated restart confirmation/disagreement
     */
    public String ConfRestart(boolean confirm);


    /**
     *
     * @return correctly formatted message to indicate that player will disconnect from server message
     */
    public String disconnect(String playerName);

    ////////////////////////    Server part messages    ////////////////////////

    /**
     *
     * @param name name of player that just joined
     * @param flag version of game that player will play
     * @return correctly formulated welcome message
     */
    public String WelcomeMsg(String name, Flags flag);

    /**
     *
     * @return question BC if every player wants to start
     */
    public String StartQBC();

    /**
     *
     * @param starting boolean if the game will start, true == starting, false == not ready yet
     * @param card colour of topcard
     * @param playersList list of all participating playernames
     * @return correctly formulated start BC
     */
    public String StartBC(boolean starting, Card card,List<String> playersList);

    /**
     *
     * @param namePlayer the player that will play
     * @param card colour of topcard
     * @return correctly formulated turn announce BC
     */
    public String TurnBC(String namePlayer, Card card);

    /**
     *
     * @param namePlayer the player that needs to play
     * @param cardsList the cards the player has in his hand
     * @return correctly formulated message for notifying a player that it's his turn
     */
    public String YourTurn(String namePlayer, List<Card> cardsList);

    /**
     *
     * @param namePlayer The player that made the play
     * @param playCard True == player played a card, false == player picked a card from the deck
     * @param card colour of card played
     * @return
     */
    public String MoveBC(String namePlayer, boolean playCard, Card card);

    /**
     *
     * @param namePlayer The player that receives a consequence
     * @param colourCard colour of card that was played
     * @param valueCard value of card that was played
     * @return correctly formulated BC to notify everyone that player receives a consequence
     */
    public String ConsequenceBC(String namePlayer, String colourCard, String valueCard);

    /**
     *
     * @param namePlayer the player that has uno
     * @return correctly formulated BC that player has uno
     */
    public String HasUnoBC(String namePlayer);

    /**
     *
     * @param namePlayer the player that won the round
     * @param hasWinner true == round is over, somebody won  false == round is still active
     * @param pointsAdded the points that the player won that round
     * @return correctly formulated BC round win message
     */
    public String RoundWinnerBC(String namePlayer, boolean hasWinner, int pointsAdded);

    /**
     *
     * @param playersList list of all participating players
     * @param pointsList list of the points per player
     * @return correctly formulated message that gives an overview on the points per player
     */
    public String PointsOverview(List<String> playersList, List<Integer> pointsList);

    /**
     *
     * @return correctly formulated question message if the players want to go for the next round
     */
    public String NxtRoundQBC();

    /**
     *
     * @param namePlayer the player that has more than 500 points
     * @return correctly formulated message that the game is over with the player that won
     */
    public String IsFinalWinnerBC(String namePlayer);

    /**
     *
     * @return correctly formulated question if the players want to restart the game
     */
    public String RestartQBC();

    String chooseColor();

    ////////////////////////        decode messages     ////////////////////////

}
