package Protocol;

import Model.Structure.Card;

import java.util.List;

public class ProtocolClass implements Protocol{

    @Override
    public String CompleteMsg(String messageContents) {
        return String.format(MSGBEGIN + SEPERATOR + messageContents + SEPERATOR);
    }

    @Override
    public String StructCard(Card card) {
        String firstPart = null;
        String secondPart = null;
        String colour = card.getColor().toString();
        String value = card.getSpecial().toString();
        int number = card.getValue();
        switch(colour){
            case "RED":
                firstPart = "R";
                break;
            case "BLUE":
                firstPart = "B";
                break;
            case "YELLOW":
                firstPart = "Y";
                break;
            case "GREEN":
                firstPart = "G";
                break;
            case "WILD":
                firstPart = "S";
                break;
            default:
                //print error
                break;
        }
        switch (value){
            case "REVERSE":
                secondPart = "R";
                break;
            case "SKIP":
                secondPart = "S";
                break;
            case "TAKE_TWO":
                secondPart = "D";
                break;
            case "COLOR":
                secondPart = "1";
                break;
            case "TAKE_FOUR":
                secondPart = "2";
                break;
//            case "NONE":
//                secondPart = null;
//                break;
            default:
                try {
                    if (number >= 0 && number < 10) {
                        secondPart = Integer.toString(number);
                    }
                    if (number >= 20 && number <= 50) {
                        break;
                    }
                } catch(NumberFormatException e){
                    DisplayError(ErrorTypes.E2);
                }
                break;
        }
        return (firstPart+secondPart);
    }

    @Override
    public String DisplayError(ErrorTypes error) {
        switch(error){
            case E1:
                System.out.println("Invalid move");
                break;
            case E2:
                System.out.println("Unknown message");
                break;
            case E3:
                System.out.println("Name is taken");
                break;
            case E4:
                System.out.println("Disconnected client");
                break;
            case E5:
                System.out.println("Flag not supported");
                break;
            case E6:
                System.out.println("Unknown card (not in hand)");
                break;
        }
        return CompleteMsg(error.name());
    }

    @Override
    public String AnnounceMsg(String name, Flags flag) {
        return CompleteMsg(String.format("Announce"+SEPERATOR+name+SEPERATOR+flag.name()));
    }

    @Override
    public String CanWeStart() {
        return CompleteMsg("CanWeStart");
    }

    @Override
    public String ConfStart(boolean confirm) {
        return CompleteMsg(String.format("CnfStart"+SEPERATOR+confirm));
    }

    @Override
    public String MyMove(Integer index) {
        return CompleteMsg(index.toString());
    }

    @Override
    public String ConfRestart(boolean confirm) {
        return CompleteMsg(String.format("ConfRestart"+SEPERATOR+confirm));
    }

    @Override
    public String disconnect(String playerName){
        return CompleteMsg("disconnect"+SEPERATOR+playerName);
    }

    @Override
    public String WelcomeMsg(String name, Flags flag) {
        return CompleteMsg("Welcome"+SEPERATOR+name+SEPERATOR+flag.name());
    }

    @Override
    public String StartQBC() {
        return CompleteMsg("Start?");
    }

    @Override
    public String StartBC(boolean starting, Card card, List<String> playersList) {
        String allNames = "";
        for(int i = 0; i < playersList.size(); i++){
            allNames = String.format(allNames+SEPERATOR+playersList.get(i));
        }
        return CompleteMsg(String.format("Starting:"+SEPERATOR+starting+SEPERATOR+StructCard(card)+SEPERATOR+allNames));
    }

    @Override
    public String TurnBC(String namePlayer, Card card) {
        return CompleteMsg(String.format("Turn"+SEPERATOR+namePlayer+SEPERATOR+StructCard(card)));
    }

    @Override
    public String YourTurn(String namePlayer, List<Card> cardsList) {
        String totalCards = "";
        for(int i = 0; i < cardsList.size(); i++){
            totalCards = String.format(totalCards + SEPERATOR + cardsList.get(i));
        }

        return CompleteMsg(String.format("YTurn"+SEPERATOR+namePlayer+SEPERATOR+totalCards));
    }

    @Override
    public String MoveBC(String namePlayer, boolean playCard, Card card) {
        String moveBC = null;
        if(playCard){
            moveBC = String.format("MoveBC"+SEPERATOR+true+SEPERATOR+StructCard(card));
        }
        else{
            moveBC = String.format("MoveBC"+SEPERATOR+true+SEPERATOR+null);
        }
        return CompleteMsg(moveBC);
    }

    @Override
    public String ConsequenceBC(String namePlayer, String colourCard, String valueCard) {
        String consequence = null;
        if(colourCard.equals("S") && valueCard.equals("2")){
            consequence ="Draw4";
        }
        else if(valueCard.equals("S")){
            consequence = "Skip";
        }
        else if(valueCard.equals("D")){
            consequence = "Draw2";
        }
        return CompleteMsg(String.format("Consequence"+SEPERATOR+namePlayer+SEPERATOR+consequence));
    }

    @Override
    public String HasUnoBC(String namePlayer) {
        return CompleteMsg("HasUNO"+SEPERATOR+namePlayer);
    }

    @Override
    public String RoundWinnerBC(String namePlayer, boolean hasWinner, int pointsAdded) {
        String roundWinnerBC = null;
        if(hasWinner){
            roundWinnerBC = String.format("RoundWinner"+SEPERATOR+true+SEPERATOR+namePlayer+SEPERATOR+pointsAdded);
        }
        else{
            roundWinnerBC = String.format("RoundWinner"+SEPERATOR+true);
        }
        return CompleteMsg(roundWinnerBC);
    }

    @Override
    public String PointsOverview(List<String> playersList, List<Integer> pointsList) {
        String combinedNamesAndPoints = null;
        for(int i = 0; i < playersList.size(); i++){
            combinedNamesAndPoints = String.format(combinedNamesAndPoints+SEPERATOR+playersList.get(i)+SEPERATOR+pointsList.get(i));
        }
        return CompleteMsg(combinedNamesAndPoints);
    }

    @Override
    public String NxtRoundQBC() {
        return CompleteMsg("NextRoundQ");
    }

    @Override
    public String IsFinalWinnerBC(String namePlayer) {
        return CompleteMsg("FinalWinner"+SEPERATOR+namePlayer);
    }

    @Override
    public String RestartQBC() {
        return CompleteMsg("RestartQ");
    }

    @Override
    public String chooseColor(){
        return CompleteMsg("ChooseColor");
    }

}
