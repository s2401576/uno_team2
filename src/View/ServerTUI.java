package View;

import Model.Server_Client.UnoServer;
import Model.Structure.Card;
import Protocol.Protocol;

import java.util.ArrayList;
import java.util.List;

public class ServerTUI{


    UnoServer server;


    public ServerTUI(UnoServer server){
        this.server = server;
    }
    public List<String> getPlayerDeckString(List<Card> cards){
        ArrayList<String> result = new ArrayList<>();
        for (Card c : cards){
            result.add(StructCard(c));
        }
        return result;
    }

    public void ready(){
        System.out.println("ready");
    }

    public String StructCard(Card card) {
        String firstPart = null;
        String secondPart = null;
        String colour = card.getColor().toString();
        String value = card.getSpecial().toString();
        int number = card.getValue();
        switch(colour){
            case "RED":
                firstPart = "R";
                break;
            case "BLUE":
                firstPart = "B";
                break;
            case "YELLOW":
                firstPart = "Y";
                break;
            case "GREEN":
                firstPart = "G";
                break;
            case "WILD":
                firstPart = "S";
                break;
            default:
                //print error
                break;
        }
        switch (value){
            case "REVERSE":
                secondPart = "R";
                break;
            case "SKIP":
                secondPart = "S";
                break;
            case "TAKE_TWO":
                secondPart = "D";
                break;
            case "COLOR":
                secondPart = "1";
                break;
            case "TAKE_FOUR":
                secondPart = "2";
                break;
//            case "NONE":
//                secondPart = null;
//                break;
            default:
                try {
                    if (number >= 0 && number < 10) {
                        secondPart = Integer.toString(number);
                    }
                    if (number >= 20 && number <= 50) {
                        break;
                    }
                } catch(NumberFormatException e){
                    DisplayError(Protocol.ErrorTypes.E2);
                }
                break;
        }
        return (firstPart+secondPart);
    }

    private void DisplayError(Protocol.ErrorTypes e2) {

    }
}