package View;

import Model.Structure.Card;
import Model.Players.Player;

import java.util.ArrayList;

public class TUI {

    public void printDeck(Player player){
        ArrayList<String> result = new ArrayList<>();
        System.out.println("Deck of " + player.getName());
        for (Card c : player.getDeck()){
            result.add(cardString(c));
            //System.out.println(cardString(c));
        }
        System.out.println(result);
    }


    public String cardString(Card card){
        String result;
        if (card.getSpecial() == Card.Special.NONE){
            result = card.getColor() + "_" + card.getValue();
        } else {
            result = card.getColor() + "_" + card.getSpecial().toString();
        }
        return result;
    }



}
