# UNO by Team_2
### Introduction

This is a project created by 2 business and IT students, 
in this project we were tasked with creating the game UNO.
The game is created for those who enjoy it :).

- **Developed by**:
    - Matthijs van der Bent _(BIT student)_
    - Xander Heinen _(BIT student)_

### Current functionality
- check whether turns are valid
- full ComputerPlayer functionality (taking, placing, etc.)
- all special cards functionality
- Full game between two computer players in UnoMainViewController with correct logic

### Gamemodes flags and explanation
- Standard UNO (s)
- Progressive UNO (P)
  - +2 and +4 cards can be stacked

### How to play the game
- Start the class "UnoServer"
- To enter the server as a Client, run the Class "Client".
- Next, as a client, announce you name by typing "announce (playername) (gamemode flag)"
- Every client should announce themselves as described above
- To start the game, write "can we start?"
- Next, each client has to agree by writing "yes", if you answer "no the game will not start"
- to play a card, simply fill in the place of the number in the list. Filling in 0 will make you draw a card.
- If you have to choose a colour after wild_Colour card of wild_draw_four simply press the colour which you want to choose
- When the final card is played, you have won and the game will automatically add up your points, winning the game.